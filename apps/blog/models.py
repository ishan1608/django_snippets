from __future__ import unicode_literals

from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
from tastypie.utils.timezone import now
from autoslug import AutoSlugField


class BlogPost(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    slug = models.SlugField(null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # For automatic slug generation
        if not self.slug:
            self.slug = slugify(self.title)[:50]

        return super(BlogPost, self).save(*args, **kwargs)


class Entry(models.Model):
    user = models.ForeignKey(User)
    pub_date = models.DateTimeField(default=now)
    title = models.CharField(max_length=200)
    # slug = models.SlugField(null=True, blank=True)
    slug = AutoSlugField(populate_from='title', unique=True, default='', editable=False)
    body = models.TextField()

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        # For automatic slug generation.
        if not self.slug:
            self.slug = slugify(self.title)[:50]

        return super(Entry, self).save(*args, **kwargs)