from django.views.generic.base import TemplateView


class IndexPageView(TemplateView):

    template_name = 'blog/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexPageView, self).get_context_data(**kwargs)
        context['message'] = 'Placeholder Index'
        return context
