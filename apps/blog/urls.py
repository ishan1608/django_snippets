from django.conf.urls import url
from .views import IndexPageView

urlpatterns = [
    # ...more URLconf bits here...
    # Index
    url(r'^$', IndexPageView.as_view(), name='index'),
]
