from django.contrib import admin
from .models import BlogPost, Entry


class EntryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'slug')


admin.site.register(Entry, EntryAdmin)
admin.site.register(BlogPost)
